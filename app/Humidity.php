<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Humidity extends Model
{
    protected $fillable = ['temperature', 'humidity'];

    protected $hidden = ['created_at', 'updated_at'];
}
