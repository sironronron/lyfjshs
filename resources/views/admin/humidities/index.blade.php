@extends('adminlte::page')

@section('title', 'Humidities')

@section('content_header')
    <h1>Humidities</h1>
@stop

@section('content')
<div class="row m-b-20">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            {{-- <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a> --}}
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered m-t-20">
 <tr>
   <th>No</th>
   <th>Humidity</th>
   <th>Temperature</th>
   <th width="280px">Action</th>
 </tr>
 @foreach ($data as $key => $user)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $user->humidity }}</td>
    <td>{{ $user->temperature }}</td>
    <td>
       <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
       <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </td>
  </tr>
 @endforeach
</table>

{!! $data->render() !!}
@stop